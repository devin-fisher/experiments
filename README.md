# Verity K8s release repo

This repo:

- provisions infrastructure needed to run Verity applications in different environments
- Installs Verity Helm charts to different environments from the CI/CD pipeline

## Provisions Verity-specific infrastructure


The Terraform code in this repo can conditionally create resources

- Dynamo DB tables
- Aurora RDS DBs
- S3 buckets
- ACM SSL certificates
- AWS IAM users to access DynamoDB/S3

that are needed to run different Verity applications (e.g. CAS, VAS, EAS) in an environment.

Conditional creation is needed since for some environments we want to re-use existing resources (e.g DBs, S3 buckets) instead of creating new ones.

Installation configuration is specified in the **tfvars** file for the environment (e.g. `live/development/devrc/terraform/vars.tfvars`).
The sample configuration looks like this:

```sh
install_apps_cfg = {
  "cas" = {
    dynamodb = {
      journal = {
        use_existing = "agency_cas"
      }
      snapshot = {
        use_existing = "agency_cas_snapshot"
      }
    }
    acm = {
      domain_name = "agency.pdev.evernym.com"
      zone_name   = "pdev.evernym.com"
    }
    s3 = {
      data-retention = {
        use_existing = "data-retention-cas-devrc"
      }
    }
  }
  "vas" = {
    dynamodb = {
      journal = {
        use_existing = "agency_vas"
      }
      snapshot = {
        use_existing = "agency_vas_snapshot"
      }
    }
    acm = {
      domain_name = "vas.pdev.evernym.com"
      zone_name   = "pdev.evernym.com"
    }
    s3 = {
      data-retention = {
        use_existing = "data-retention-vas-devrc"
      }
    }
  }
} #change

install_common_cfg = {
  shortener = {
    s3 = {
      bucket_name = "s3shortener-pdev-evernym-com"
    }
    acm = {
      domain_name = "s3shortener.pdev.evernym.com"
      zone_name   = "pdev.evernym.com"
    }
  }
} #change
```


## Vault
Vault has been configured to allow JWT auth from this repo, rather than static tokens that need to be renewed or expire every 2 weeks.
- one of the following needs to exist in the ci/cd job to authenticate:
```
export VAULT_TOKEN=$(vault write -field=token auth/gitlab.com/login role=${CI_PROJECT_NAME} jwt=${CI_JOB_JWT})
```
or
```
export VAULT_TOKEN=$(curl -s -X POST --data "{\"jwt\":\"${CI_JOB_JWT}\",\"role\":\"${CI_PROJECT_NAME}\"}" ${VAULT_ADDR}/v1/auth/gitlab.com/login | jq -r ".auth.client_token")
```
- https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
- If another project needs similar access, ask someone from TE to run `gitlab_vault.sh` from the ops-tools repo

## Scripts
Extra helper scripts can be found in the [scripts](scripts/README.md) directory

## Installing Verity Helm charts

In principal, Verity helm charts should be installed only from the CI/CD pipeline of this repo.

The pipeline is configured to automatically install the latest Verity version to the DevRC environment on each merge to `main` of the `verity` repo (there is a pipeline trigger configured from the `verity` to the `verity-k8s-release` repo) 

It is also possible to manually install certain Verity version in environments. For this, it is needed to:
- run the pipeline on the `master` branch of the `verity-k8s-repo`
- Start manual jobs to deploy to an environment from this pipeline. It is necessary to enter the parameter `VERITY_APPLICATION_VERSION` when starting the manual job

### Instaling Verity Helm charts from the local machine

When working extensively on the pipeline development or changes to the Verity helm templates, it is possible to initiate Verity Helm charts deployment to an environment from the local machine of the privileged user.

For this, it is needed to set up local environment accordingly:

1. Create personal access token with the scope `api` on your Gitlab account
2. Add Verity Gitlab Helm package repository on your local host:
```sh
$ helm repo add --username <your_gitlab_handle> --password <token_from_step_1> verity https://gitlab.com/api/v4/projects/26909221/packages/helm/stable
```
3. Login to Vault locally with the user that has enough permissions to read from `secrets/kubernetes_secrets/`
```sh
export VAULT_ADDR=https://vault.corp.evernym.com:8200
$ vault login -method=ldap username=<your_username>
```

4. Specify Kubernetes namespace to which you want to deploy via the `HELM_OPTIONS` environment variable
```sh
$ export HELM_OPTIONS="--namespace verity-team1"
```

5. Run the `helm_helper.sh` script from the root of your local clone of the `verity-k8s-release` repo, e.g.:
```sh
$ ./cicd/helm_helper.sh vas development/team1 0.4.135538615.a010ad6
```

This will install the Verity application (parameter $1) in the specified environment (parameter $2) with the specified version (parameter $3) in the namespace specified via the `HELM_OPTIONS` using the latest Verity helm chart from the Verity Gitlab Helm package repository.

If you want to deploy using your local version of the Verity Helm templates then before starting the `helm_helper.sh` script export the environment variable `VERITY_HELM_REPO` to point to the `/verity/src/helm` folder of your local clone of the `verity` repo, e.g.
```sh
$ export VERITY_HELM_REPO=/home/repos/verity/verity/src/helm
$ ./cicd/helm_helper.sh vas development/team1 0.4.135538615.a010ad6
```
