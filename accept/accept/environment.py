import os

import pyhocon


def find_config_file(file_name):
    file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../config/", file_name))
    if os.path.exists(file_path):
        return file_path
    else:
        raise Exception(f'Unable to file configuration for "{file_name}"')


def get_env_config():
    target_env = os.environ.get("TARGET_ENV")
    if target_env:
        config_file = find_config_file(target_env+"_env.conf")
        return pyhocon.ConfigFactory.parse_file(config_file)
    else:
        raise Exception('Environment variable "TARGET_ENV" is not set. It must be set to continue')


def get_test_env_config():
    os.environ["TARGET_ENV"] = "autoreleasetest"

    config = """
    {
    }
    """
    common_path = find_config_file("common.conf")
    common = pyhocon.ConfigFactory.parse_file(common_path)
    return pyhocon.ConfigFactory.parse_string(config).with_fallback(common)
