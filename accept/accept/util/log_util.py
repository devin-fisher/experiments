import coloredlogs


def init_colored_stdout_logging(level='DEBUG'):
    field_styles = coloredlogs.DEFAULT_FIELD_STYLES.copy()
    field_styles['levelname'] = dict(color=30, bold=True)
    coloredlogs.install(
        level=level,
        field_styles=field_styles,
        datefmt="%H:%M:%S",
        fmt="%(levelname)8s %(asctime)s %(filename)15s(%(lineno)3d) -- %(message)s"
    )


def log_variables(logger, **kwargs):
    logger.info(f"== Variables ==")
    for k, v in kwargs.items():
        logger.info(f'  {k}: {v}')
