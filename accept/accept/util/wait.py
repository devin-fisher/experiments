from time import time as now
from time import sleep


def await_condition(condition_fun, max_duration_sec, interval_sec, no_throw=False):
    stop = now() + max_duration_sec
    while True:
        if condition_fun() is not True:
            current = stop - now()
            if current <= 0:
                if no_throw:
                    return False
                else:
                    raise Exception(f"Waiting has timed out ({max_duration_sec})")
            else:
                sleep(interval_sec)
        else:
            return True
