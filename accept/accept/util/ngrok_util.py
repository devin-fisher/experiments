import json
import logging
import subprocess
import os
import time

from accept.util.process_util import run_process
from accept.util.wait import await_condition

log = logging.getLogger(__file__)


def setup_ngrok():
    ngrok = subprocess.Popen(['ngrok', 'start', '--all', '--config=/etc/ngrok/ngrok.yaml', '--log=stdout'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    log.info('ngrok process pid: ' + str(ngrok.pid))

    def ngrok_url():
        output = subprocess.run(['curl', '-s', 'localhost:4040/api/tunnels'], check=True, stdout=subprocess.PIPE)
        endpoints = json.loads(output.stdout.decode('utf-8').strip())['tunnels']
        log.info(endpoints)
        try:
            invite_endpoint = list(filter(lambda x: x['name'] == 'inviteurl', endpoints))[0]['public_url']
            log.info(invite_endpoint)
        except (IndexError, subprocess.SubprocessError) as e:
            invite_endpoint = None
            pass
        try:
            sample_app_webhook_endpoint = list(filter(lambda x: x['name'] == 'webhook', endpoints))[0]['public_url']
            log.info(sample_app_webhook_endpoint)
        except (IndexError, subprocess.SubprocessError) as e:
            sample_app_webhook_endpoint = None
            pass
        return invite_endpoint, sample_app_webhook_endpoint

    def ngrok_is_started():
        try:
            ngrok_url()
            return True
        except (IndexError, subprocess.SubprocessError) as e:
            pass

        return False

    # await_condition(ngrok_is_started, 30, 0.5)
    time.sleep(15) # debug
    invite_endpoint, sample_app_webhook_endpoint = ngrok_url()
    log.info(f'ngrok URLs: {invite_endpoint} {sample_app_webhook_endpoint}')
    return invite_endpoint, sample_app_webhook_endpoint

    
def stop_ngrok():
    run_process(['pkill', 'ngrok'], ignore_results=True)
