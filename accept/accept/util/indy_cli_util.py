import logging
import sys

import pexpect

log = logging.getLogger(__file__)
HAS_TTY = sys.stdout.isatty()


def start_cli(pool_name, wallet_name, endorser_did, endorser_seed, genesis_file, config_file, large_send=False, print_out=False):
    inTTY = "" if HAS_TTY else "NOT"
    log.info(f"Running {inTTY} in a TTY")

    if large_send:
        log.info("Large send support requested. Disabling 'Canonical input processing'")
        cli = pexpect.spawn(
            '/bin/bash',
            encoding='utf-8',
            logfile=sys.stdout if print_out else None
        )
        cli.delaybeforesend = 1
        cli.sendline('stty -icanon')
        cli.sendline(f'vdrtools-cli --config {config_file}')
    else:
        cli = pexpect.spawn(
            'vdrtools-cli',
            ['--config', f'{config_file}'],
            encoding='utf-8',
            logfile=sys.stdout if print_out else None
        )
        cli.delaybeforesend = 1

    log.info(f'Creating pool {pool_name}')
    cli.expect('"at_submission" is used as transaction author agreement acceptance mechanism')
    cli.sendline(f'pool create {pool_name} gen_txn_file={genesis_file}')
    cli.expect(f'Pool config "{pool_name}" has been created')

    log.info(f'Connecting to pool {pool_name}')
    cli.sendline(f'pool connect {pool_name}')
    cli.expect(f'Pool "{pool_name}" has been connected')
    cli.expect('Would you like to read it?')
    if HAS_TTY:
        cli.sendline('y')
    cli.expect('Would you like to accept it?')
    if HAS_TTY:
        cli.sendline('y')
    cli.expect('Transaction Author Agreement has been accepted.')
    log.info(f'Connected to pool {pool_name}')

    log.info(f'Creating wallet {wallet_name}')
    cli.sendline(f'wallet create {wallet_name} key=1')
    cli.expect(f'Wallet "{wallet_name}" has been created')
    cli.sendline(f'wallet open {wallet_name} key=1')
    cli.expect(f'Wallet "{wallet_name}" has been opened')
    log.info(f'Opened wallet {wallet_name}')

    log.info(f'Creating did {endorser_did}')
    cli.sendline(f'did new seed={endorser_seed}')
    cli.expect(f'Did "{endorser_did}" has been created with ".*" verkey')
    cli.sendline(f'did use {endorser_did}')
    cli.expect(f'Did "{endorser_did}" has been set as active')
    log.info(f'Using did {endorser_did}')

    log.info(f'Indy CLI is started')
    return cli


def write_nym(did, verkey, pool_name, wallet_name, endorser_did, endorser_seed, genesis_file, config_file, print_out=False):
    large_send = False
    indy_cli = start_cli(pool_name, wallet_name, endorser_did, endorser_seed, genesis_file, config_file, large_send, print_out)
    log.info(f'Writing did {did} to ledger')
    indy_cli.sendline(f'ledger nym did="{did}" verkey="{verkey}"')
    indy_cli.expect('DID .NYM. request has been sent')
    indy_cli.expect('Metadata:')  # response for ledger
    log.info(f'Did {did} written to ledger')
    indy_cli.terminate()


def endorse_txn(needs_endorsement_txn, pool_name, wallet_name, endorser_did, endorser_seed, genesis_file, config_file, print_out=False):
    large_send = False
    if len(needs_endorsement_txn) > 3000:
        large_send = True
    indy_cli = start_cli(pool_name, wallet_name, endorser_did, endorser_seed, genesis_file, config_file, large_send, print_out)
    log.info(f'Endorsing txt to ledger')
    indy_cli.sendline(f'ledger endorse txn="{needs_endorsement_txn}"')
    indy_cli.expect('Transaction has been sent')
    indy_cli.expect('Metadata:')  # response for ledger
    log.info(f'Txt written to ledger')
    indy_cli.terminate()
