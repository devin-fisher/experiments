import logging
import os

from git import Repo

log = logging.getLogger(__file__)


def checkout_repo(url, to_dir, commit_hash=None):
    already_exists = os.path.exists(to_dir) and \
                     url in map(lambda r: list(r.urls)[0], Repo.init(to_dir).remotes)

    log.info(f"checking out repo to {to_dir}")
    if already_exists:
        log.info(f"{to_dir} repo already existed")
        repo = Repo.init(to_dir)
    else:
        repo = Repo.clone_from(
            url,
            to_dir,
            multi_options=['--single-branch']
        )

    if commit_hash is not None:
        repo.create_head(commit_hash, commit=commit_hash).checkout()

    log.info(f"finished checking out repo")