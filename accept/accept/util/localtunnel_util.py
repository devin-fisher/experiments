import pexpect
import logging

log = logging.getLogger(__file__)

def setup_localtunnel(port):
    prefix = "your url is: "
    tunnel = pexpect.spawn(f"lt -p {port}", encoding="utf-8")
    tunnel.expect(f"{prefix}(.+?).lt")
    log.info(tunnel.after)

    return tunnel, tunnel.after.replace(prefix, "")


def stop_localtunnel(tunnel):
    tunnel.close()
