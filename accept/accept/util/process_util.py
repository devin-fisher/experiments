import logging
import subprocess

log = logging.getLogger(__file__)


def run_process(action, ignore_results=False, cwd=None):
    try:
        log.info('Running action: ' + str(action))
        subprocess.run(action, check=True, cwd=cwd)
    except subprocess.CalledProcessError as e:
        if ignore_results:
            return

        log.error('Action failed to run successfully!')
        log.error(str(e))
        raise e
