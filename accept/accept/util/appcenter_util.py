import logging
import os
import requests

base_url = 'https://api.appcenter.ms/v0.1/apps/Evernym-Inc/QA-MeConnect-Android/releases/'
default_filename = 'app-arm64-v8a-release.apk'

log = logging.getLogger(__file__)


def _build_headers(key):
    return {
        'X-API-Token': key,
        'content-type': 'application/json'
    }


def download_apk(key, url=base_url, file=default_filename):
    headers = _build_headers(key)
    # arm64_release_ids = [
    #     requests.get(url, headers=headers).json()[i]['id'] for i in range(4)
    # ]

    # use 1.7.1 here and then use stable CM builds instead of master ones,
    # don't use prepared appcenter link to avoid signature issue!
    arm64_release_ids = [2614, 2615, 2616, 2617]
    log.info("Release Ids: " + str(arm64_release_ids))
    arm64_download_urls = [
        requests.get(
            url + str(arm64_release_id),
            headers=headers
        ).json()['download_url'] for arm64_release_id in arm64_release_ids
    ]
    log.info("Urls: " + str(arm64_download_urls))
    for arm64_download_url in arm64_download_urls:
        if file in arm64_download_url:
            r = requests.get(arm64_download_url, allow_redirects=True)
            log.info("Downloaded apk: " + str(file))
            open(file, 'wb').write(r.content)

    return file
