"""
Code to facilitate the testing of a Verity deployment
"""
import logging
import os
from os.path import join

from accept.environment import get_env_config
from accept.util.git_util import checkout_repo
from accept.util.log_util import init_colored_stdout_logging
from accept.util.ngrok_util import setup_ngrok, stop_ngrok
from accept.util.process_util import run_process

log = logging.getLogger(__file__)


def set_env_var(key, value):
    log.info(f'Setting EnvVar -- {key}:{value}')
    os.environ[key] = value


def check_out_verity(verity_dir, package_version):
    verity_commit_hash = package_version.split('.')[-1]

    if len(str(package_version).strip()) == 0:
        raise Exception(f'Commit hash was not parsed from "{package_version}"')

    checkout_repo(
        'https://gitlab.com/evernym/verity/verity.git',
        verity_dir,
        commit_hash=verity_commit_hash
    )


def setup_conf(test_stage_dir, integration_config_dir, integration_tests_dir):
    actions = [
        ['cp', join(test_stage_dir, 'config', 'environment.conf'), join(integration_config_dir, 'environment.conf')],
        ['cp', join(test_stage_dir, 'config', 'integration.conf'), join(integration_config_dir, 'values.conf')],
    ]
    for action in actions:
        run_process(action)


def setup_env(env_conf):
    package_version = env_conf['package_version']
    if len(str(package_version).strip()) == 0:
        raise Exception('Verity Version is required to run these tests')

    log.info(f'Using Verity version -- "{package_version}"')

    ci_project_dir = os.environ['CI_PROJECT_DIR']
    log.info(f'Using CI_PROJECT_DIR -- "{ci_project_dir}"')
    os.chdir(ci_project_dir)

    verity_release_dir = join(ci_project_dir, 'accept')

    verity_dir = join(ci_project_dir, 'verity')
    integration_config_dir = join(verity_dir, 'integration-tests', 'src', 'test', 'resources')
    integration_tests_dir = join(verity_dir, 'integration-tests', 'src', 'test', 'scala', 'com', 'evernym',
                                 'integrationtests', 'e2e', 'apis')
    test_stage_dir = join(verity_release_dir, 'accept', 'integration')
    genesis_data_dir = os.path.join(ci_project_dir, "accept", "config", "genesis")

    check_out_verity(verity_dir, package_version)
    setup_conf(test_stage_dir, integration_config_dir, integration_tests_dir)
    _, ngrok_url = setup_ngrok()

    set_env_var('NGROK_URL', ngrok_url)
    log.info(f'Using NGROK_URL -- "{ngrok_url}"')
    set_env_var('GENESIS_DATA_DIR', genesis_data_dir)
    log.info(f'Using GENESIS_DATA_DIR -- "{genesis_data_dir}"')


def run_tests(env_conf):
    print("*"*5 + os.getcwd())

    os.chdir('verity')

    # test_suites = [
    #     'integrationTests/testOnly com.evernym.integrationtests.e2e.apis.JavaSdkFlowSpec -- -n com.evernym.integrationtests.e2e.tag.annotation.Integration',
    # ]
    # for test_suite in test_suites:
    #     run_process(['sbt', test_suite])
    run_process(['sbt', 'version'])


def teardown_env(env_conf):
    stop_ngrok()


def main(_, env_conf=None):
    if env_conf is None:
        # env_conf = get_env_config()
        pass
    try:
        # setup_env(env_conf)
        run_tests(env_conf)
    finally:
        # teardown_env(env_conf)
        pass


if __name__ == '__main__':
    init_colored_stdout_logging(level='DEBUG')
    main(None)
