#!/usr/bin/env python3
import argparse
import logging
import os
import sys

import coloredlogs

from accept.util.log_util import init_colored_stdout_logging

module_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, module_path)

from accept import acceptance, integration

# Using coloredlogs to stdout for console output. Avoid using print()
log = logging.getLogger(__file__)
init_colored_stdout_logging(level="INFO")


def parse_args(args=None):
    parser = argparse.ArgumentParser(description='Release for Verity')
    parser.add_argument("cmd")
    parser.add_argument("sub", nargs='?')
    # It was a design decision to not have other parameters to the cmd
    # Parameters should be added as ENV VARIABLES
    # This way it is easier to control in gitlab CI/CD pipelines
    args = parser.parse_args(args=args)
    log.debug("Args: " + str(args))
    return args


def main(args):
    if args.cmd == 'integration':
        log.debug("Invoking 'integration' cmd")
        integration.main(args)
    elif args.cmd == 'acceptance-verity':
        log.debug("Invoking 'acceptance-verity' cmd")
        acceptance.main(args)
    elif args.cmd == 'acceptance-vcx':
        log.debug("Invoking 'acceptance-vcx' cmd")
        acceptance.main(args)
    else:
        log.error("Unknown cmd -- " + args.cmd)


if __name__ == "__main__":
    log.info("Starting...")
    main(parse_args())
