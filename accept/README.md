# verity-release

## Concepts
### **package**
The built artifact of Verity that will be promoted and deployed and tested. For any given pipeline run, there should 
only be one and only one package considered. This package should be a release candidate and ready to be deployed. But 
this pipeline will not be a ware of any vetting done prior to this pipeline.

Currently this *package* a Debian package but that should be heavy assumption. In the future it could be a docker 
container for K8S.

### **version**
The version of the package used in the pipeline. 

### **environment**
Environments are a set of managed (typically by TE) resources. These environments typically consist of 
three applications and the resources needed for these applications. 

Example of environments are:
* `team1`
* `team2`
* `devrc`
* `staging`
* `prod`
 
#### Target Environments 
The Target Environment is the environment that is targeted by this CD Pipeline.  The package is deployed to 
this environment. 

A successful pipeline will result in the RC package being deployed to the target environment.

A failed pipeline could also result in the RC package being deployed to the target environment. Depending on 
the job that failed. Rolling back is not in scope but will be a future development.

### **repo**
Is the artifact repo that contain Verity packages.

Currently repos will contain Debian package but again that should not be a heavy assumption

#### origin repo
The repository from which the package is promoted.

#### target repo
The repository to which the package is promoted.

## Workflow
Each verity master pipeline has *deploy-master* step that automatically creates downstream verity-release pipeline to deploy master build to *devrc* environment and *deploy-manually-team1* step that can create downstream verity-release pipeline manually to deploy master to *team1* environment.
Extra deployment steps can be added in verity's .gitlab-ci.yml in *deploy* step to deploy master build to other environments.
Verity-release uses *auto-deploy* branch for deployment from verity master.
Successful pipeline (promotion, deployment and testing jobs are green) has *deploy-staging* job that can be run manually to deploy verity to staging environment.
You can deploy any custom changes other than verity-application version to the environment by passing your *control-repo fork ID* and *branch name* to pipeline (see `Pipeline steps` section).

### Pipeline steps
* Verity-test - runs subset of verity integration tests against upgraded environment.

### Manual run
* Create new branch from auto-deploy
* Mark this branch as *protected* in gitlab settings
* Remove *workflow* section from  .gitlab-ci.yml
* Add `TARGET_ENV` and `VERITY_VERSION` variables to .gitlab-ci.yml and set their values
* Commit and push changes to run pipeline

### Environmental variables
* PACKAGE_NAME - package name (verity-application)
* VERITY_VERSION - package version
* TARGET_ENV - environment for deployment ( team1 | devrc | staging ), it is passed from verity pipeline to auto-deploy branch automatically and must be set manually in other branches.
