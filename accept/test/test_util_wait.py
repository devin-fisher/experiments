import pytest

from accept.util.wait import await_condition


def test_await_condition():
    def t():
        return True
    def f():
        return False

    await_condition(t, 2, 1)
    with pytest.raises(Exception):
        await_condition(f, 2, 1)

    # not sure how to do a test that starts false and turns to true
