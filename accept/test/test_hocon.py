import pyhocon
import os

from os.path import dirname, join
from os import walk


def test_env_variables():
    config = """
    {
        test = foo
        test = ${?TEST_ENV_VAR}
        
    }
    """
    test_conf = pyhocon.ConfigFactory.parse_string(config)
    assert test_conf["test"] == "foo"
    os.environ["TEST_ENV_VAR"] = "bar"
    test_conf = pyhocon.ConfigFactory.parse_string(config)
    assert test_conf["test"] == "bar"


def test_current_files():
    root_dir = dirname(dirname(__file__))
    conf_dir = join(root_dir, "config")

    for (dir_name, _, conf_files) in walk(conf_dir):
        for c in conf_files:
            rendered = pyhocon.ConfigFactory.parse_file(join(dir_name, c))
            assert rendered is not None
        break
